#!/usr/bin/env python
# -*- coding: utf-8 -*-

from cortextlib.legacy import containerize

import os, sys
reload(sys) 
sys.setdefaultencoding("utf-8")
from sqlite3 import *
import types
from librarypy.path import *
from librarypy import fonctions
import logging, pprint

#############################################################################
#############################################################################
##########################-LOADING PARAMETERS-###############################
#############################################################################
#############################################################################
print 'user_parameters',user_parameters


parameters_user=fonctions.load_parameters(user_parameters)
result_path=parameters_user.get('result_path','')
result_path0=result_path[:]
fonctions.progress(result_path,0)

data_tables = parameters_user.get('data_table',['ISIpubdate'])
print "parameters_user.get('nb_bins',1)",parameters_user.get('nb_bins',1)
try:
	nb_period0=int(parameters_user.get('nb_bins',1))
except:
	try:
		nb_period0=(parameters_user.get('nb_bins',1))
	except:
		nb_period_range = parameters_user.get('nb_bins','')
		eval('nb_period0 = ' + nb_period_range)

time_cut_type=parameters_user.get('data_cut_type','homogeneous')
print 'time_cut_type',time_cut_type
print 'nb_period0',nb_period0

corpus_file=parameters_user.get('corpus_file','corpus_file')


#################################
###logging user parameters#######
#################################
logging.basicConfig(filename=os.path.join(result_path,'.user.log'), filemode='w', level=logging.DEBUG,format='%(asctime)s %(levelname)s : %(message)s', datefmt="%Y-%m-%d %H:%M:%S")
logging.info('Data Slicer Started')
yamlfile = 'data_slicer.yaml'
if 'cointet' in os.getcwd():
	yamlfile = '/Users/jpcointet/Desktop/cortext/manager/scripts/data_slicer/'+yamlfile
parameterslog=fonctions.getuserparameters(yamlfile,parameters_user)
logging.info(parameterslog)
fonctions.progress(result_path0,2)
##################################
### end logging user parameters###
##################################
print 'parameters:'
print 'corpus_file',corpus_file
bdd_name=corpus_file
conn,curs=fonctions.create_bdd(bdd_name)
print 'nb_period0',nb_period0

nb_period=nb_period0
print 'nb_period',nb_period
nb_period_bis=[]
try:
	if nb_period[0]=='[':
		print 'string style'
		nb_period=nb_period.replace(']','').replace('[','')
		parts = nb_period.split(';')
		for part in parts:
			pa=[]
			print 'part', part
			partv=part.split(',')
			for x in partv:
				par = map(lambda x: int(x),x.split(':'))
				for x in range(par[0],par[-1]+1):
					pa.append(x)
			print pa 
			nb_period_bis.append(pa)
	else:
		nb_period_bis=nb_period	
except:
	nb_period_bis=nb_period0
print 'nb_period_bis',nb_period_bis
nb_period0=nb_period_bis

print 'data_tables',data_tables
if 1:
	data_table= data_tables
	logging.info('Reading data from table '+ data_table)
	fonctions.progress(result_path0,4)
	

	years_bins_dict,result_pubdate,years_cover_dist = fonctions.cut_data(curs,nb_quantile=nb_period0,cut_type=time_cut_type,period_size=1,overlap_size=False,time_table=data_table)
	#print 'result_pubdate',result_pubdate
	logging.info('Data distribution'+ str(years_cover_dist))
	
	
	nb_period=len(years_bins_dict.keys())
	years_bins_dict_inv=fonctions.inverse_dict_year(years_bins_dict)
	logging.info('The following groups of values have been created'+ pprint.pformat(years_bins_dict.keys()))
	
	fonctions.progress(result_path0,24)

	conn,curs=fonctions.create_bdd(bdd_name)
	new_table_name  = data_table + '_' + time_cut_type[:3]  + '_' + str(nb_period)
	fonctions.create_table(new_table_name,conn,curs)
	results = curs.execute(" SELECT file,id,rank,parserank,data FROM "+data_table  ).fetchall()#cited journal
	results_sql = []
	#print "years_bins_dict_inv",years_bins_dict_inv
	for x in results:
		x=list(x)
		x[-1]=years_bins_dict_inv[int(x[-1])][0]
		results_sql.append(x)
	fonctions.remplir_table_fast(bdd_name,new_table_name,results_sql,champs_name="(file, id , rank , parserank , data)")
	print 'a new table, ', new_table_name, ' has been created featuring the following values: ', years_bins_dict.keys()
	logging.info( 'A new table, '+new_table_name + ' has been created')# featuring the following values: '+ pprint.pformat(years_bins_dict.keys()))
	fonctions.progress(result_path0,100)
	
#sys.path.append("../")
from librarypy import descriptor
descriptor.generate(bdd_name)
logging.info('Data successfully binned')
fonctions.progress(result_path0,100)

# try:
# 	cli_path = "../../cli/"
# 	result_path_yaml_out=result_path
# 	(result_path_yaml_out,tail)= os.path.split(result_path)
# 	(result_path_yaml_out2,tail2)= os.path.split(result_path_yaml_out)
# 	yaml_dict={}
# 	yaml_dict['uri'] = tail2
# 	yaml_dict['description'] = 'Heterogeneous network'
# 	yaml_dict['structure'] = 'network'
# 	yaml_dict['type'] = 'json'
# 	yaml_dict['version']={}
# 	yaml_dict['version']['major']=int(tail)
# 	yaml_dict['version']['minor']=0
# 	import json
# 	cli_command = 'php ' + cli_path + "cortextcli.php dataset add "  + result_path_yaml_out
# 	cli_command = cli_command  + " '"+json.dumps(yaml_dict) + "'"
# 	print cli_command
# 	print os.system(cli_command)
# except:
# 	pass
#
