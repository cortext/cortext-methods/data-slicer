# CorTexT Data Slicer

CorTexT Data Slicer is a method in [CorTexT Manager](https://docs.cortext.net/) that simply slices numeric data (provided that they are integer values) into any given number of quantiles (to be chosen in the form).

For more usage details, see the method's [user documentation](https://docs.cortext.net/data-slicer/).

## Local deployment

- Create a symbolic link to [cortextlib/src/cortextlib](https://gitlab.com/cortext/cortext-methods/cortextlib/-/tree/master/src/cortextlib?ref_type=heads) in the root folder of this project.
- Inside the vagrant run the commands below to update the list of scripts in the local CorText Manager database.
  - `sudo mysql -u root ct_manager < /srv/cortext/cortext-manager/data/table-script-datas.sql`
  - `sudo mysql -u root ct_manager < /srv/setup/config_files/cortext/cortext-methods/table-script-datas.sql`
- Build the docker image inside vagrant in `/srv/cortext/cortext-methods-transition/data-slicer`: `docker build -t cortext-methods/data-slicer .`

## License

Copyright (C) 2024 CorTexT

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
